# ARTS--a week
### A: Algorithm:  to solve a leetcode problem
### R: Review: to review a English technical article
### T: Tip: to learn a small technical tip
### S: Share: to share a article which has it's view and perspective

## LeetCode

| #  | Title | Solution | Difficulty|
|:--:|:-----|:--------:|:---------:|
|1|[Two Sum](https://leetcode.com/problems/two-sum/description/)|[Java](src/array/twoSum/TwoSum.java)|Easy|
|2|[Add Two Numbers](https://leetcode.com/problems/add-two-numbers/description/)|[Java](src/linkedList/addTwoNumbers/AddTwoNumbers.java)|Medium|
|3|[Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/description/)|[Java](src/string/longestSubstring/LongestSubstring.java)|Medium|
|9|[Palindrome Number](https://leetcode.com/problems/palindrome-number/description/)|[Java](src/math/palindromeNumber/PalindromeNumber.java)|Easy|
|13|[Roman To Integer](https://leetcode.com/problems/roman-to-integer/description/)|[Java](src/string/romanToInteger/RomanToInteger.java)|Easy|
|14|[Longest Common Prefix](https://leetcode.com/problems/longest-common-prefix/description/)|[Java](src/string/longestCommonPrefix/LongestCommonPrefix.java)|Easy|
|20|[valid parentheses](https://leetcode.com/problems/valid-parentheses/description/)|[Java](src/string/validParentheses/ValidParentheses.java)|Easy|
|21|[Merge Two Sorted Lists](https://leetcode.com/problems/merge-two-sorted-lists/description/)|[Java](src/linkedList/mergeTwoSortedLists/MergeTwoSortedLists.java)|Easy|
|26|[Remove Duplicates](https://leetcode.com/problems/remove-duplicates-from-sorted-array/description/)|[Java](src/array/removeDuplicates/RemoveDuplicates.java)|Easy|
|27|[Remove Element](https://leetcode.com/problems/remove-element/description/)|[Java](src/array/removeElement/RemoveElement.java)|Easy|
|28|[Implement Str](https://leetcode.com/problems/implement-strstr/description/)|[Java](src/string/implementStrStr/ImplementStr.java)|Easy|
|35|[Search Insert Position](https://leetcode.com/problems/search-insert-position/description/)|[Java](src/array/searchInsertPosition/SearchInsertPosition.java)|Easy|
|38|[Count and Say](https://leetcode.com/problems/count-and-say/description/)|[Java](src/string/countAndSay/CountAndSay.java)|Easy|
|53|[Maximum Subarray](https://leetcode.com/problems/maximum-subarray/description/)|[Java](src/array/maximumSubarray/MaximumSubarray.java)|Easy|
|58|[Length of Last Word](https://leetcode.com/problems/length-of-last-word/description/)|[Java](src/string/lengthOfLastWord/LengthOfLastWord.java)|Easy|
|66|[Plus One](https://leetcode.com/problems/plus-one/description/)|[Java](src/array/plusOne/PlusOne.java)|Easy|
|67|[Add Binary](https://leetcode.com/problems/add-binary/description/)|[Java](src/string/addbinary/AddBinary.java)|Easy|
|69|[Sqrt(x)](https://leetcode.com/problems/sqrtx/description/)|[Java](src/math/sqrtofx/SqrtOfX.java)|Easy|
|70|[Climbing Stairs](https://leetcode.com/problems/climbing-stairs/description/)|[Java](src/dynamicProgram/climbingStairs/ClimbingStairs.java)|Easy|
|83|[Remove Duplicates from Sorted List](https://leetcode.com/problems/remove-duplicates-from-sorted-list/description/)|[Java](src/linkedList/removeDuplicatesfromSortedList/RemoveDuplicatesfromSortedList.java)|Easy|
|88|[Merge Sorted Array](https://leetcode.com/problems/merge-sorted-array/description/)|[Java](src/array/mergeSortedArray/MergeSortedArray.java)|Easy|
|100|[Same Tree](https://leetcode.com/problems/same-tree/description/)|[Java](src/tree/sameTree/SameTree.java)|Easy|
|101|[Symmetric Tree](https://leetcode.com/problems/symmetric-tree/description/)|[Java](src/tree/SymmetricTree.java)|Easy|
|104|[Maximum Depth of Binary Tree](https://leetcode.com/problems/maximum-depth-of-binary-tree/description/)|[Java](src/tree/maximumDepthofBinaryTree/MaximumDepthofBinaryTree.java)|Easy|
|107|[Binary Tree Level Order Traversal II](https://leetcode.com/problems/binary-tree-level-order-traversal-ii/description/)|[Java](src/tree/binaryTreeLevelOrderTraversal_2/BinaryTreeLevelOrderTraversal.java)|Easy|
|108|[Convert Sorted Array to Binary Search Tree](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/description/)|[Java](src/tree/convertSortedArraytoBinarSearchTree/ConvertSortedArraytoBinarySearchTree.java)|Easy|
|110|[Balanced Binary Tree](https://leetcode.com/problems/balanced-binary-tree/description/)|[Java](src/tree/balancedBinaryTree/BalancedBinaryTree.java)|Easy|
|111|[Minimum Depth of Binary Tree](https://leetcode.com/problems/minimum-depth-of-binary-tree/)|[Java](src/tree/minimumDepthofBinaryTree/MinimumDepthofBinaryTree.java)|Easy|
|112|[Path Sum](https://leetcode.com/problems/path-sum/description/)|[Java](src/tree/pathSum/PathSum.java)|Easy|
|118|[Pascal's Triangle](https://leetcode.com/problems/pascals-triangle/description/)|[Java](src/array/pascalTriangle/PascalTriangle.java)|Easy|
|119|[Pascal's Triangle II](https://leetcode.com/problems/pascals-triangle-ii/description/)|[Java](src/array/pascalTriangle/PascalTriangle_2.java)|Easy|
|121|[Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description/)|[Java](src/array/bestTimetoBuyandSellStock/BestTimetoBuyandSellStock.java)|Easy|
|122|[Best Time to Buy and Sell Stock II](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/description/)|[Java](src/array/bestTimetoBuyandSellStock/BestTimetoBuyandSellStock_2.java)|Easy|
|125|[Valid Palindrome](https://leetcode.com/problems/valid-palindrome/)|[Java](src/string/validPalindrome/ValidPalindromeTest.java)|Easy|
|136|[Single Number](https://leetcode.com/problems/single-number/)|[Java](src/hashtable/SingleNumber.java)|Easy|
|141|[Linked List Cycle](https://leetcode.com/problems/linked-list-cycle/)|[Java](src/linkedList/LinkedListCycle/LinkedListCycle.java)|Easy|
|155|[Min Stack](https://leetcode.com/problems/min-stack/)|[Java](src/stack/MinStack.java)|Easy|
|160|[Intersection of Two Linked Lists](https://leetcode.com/problems/intersection-of-two-linked-lists/)|[Java](src/linkedList/intersectionofTwoLinkedLists/IntersectionofTwoLinkedLists.java)|Easy|
|167|[Two Sum II - Input array is sorted](https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/)|[Java](src/array/twoSum2/TwoSum2.java)|Easy|
|168|[Excel Sheet Column Title](https://leetcode.com/problems/excel-sheet-column-title/)|[Java](src/math/excelSheetColumnTitle/ExcelSheetColumnTitle.java)|Easy|
|169|[Majority Element](https://leetcode.com/problems/majority-element/)|[Java](src/array/majorityElement/MajorityElement.java)|Easy|
|171|[Excel Sheet Column Number](https://leetcode.com/problems/excel-sheet-column-number/)|[Java](src/math/excelSheetColumnNumber/ExcelSheetColumnNumber.java)|Easy|
|172|[Factorial Trailing Zeroes](https://leetcode.com/problems/factorial-trailing-zeroes/)|[Java](src/math/factorialTrailingZeroes/FactorialTrailingZeroes.java)|Easy|
|189|[Rotate Array](https://leetcode.com/problems/rotate-array/)|[Java](src/array/rotateArray/RotateArray.java)|Easy|
|217|[Contains Duplicate](https://leetcode.com/problems/contains-duplicate/)|[Java](src/array/containsDuplicate/ContainsDuplicate.java)|Easy|
|219|[Contains Duplicate II](https://leetcode.com/problems/contains-duplicate-ii)|[Java](src/array/containsDuplicate2/ContainsDuplicate.java)|Easy|
|268|[Missing Number](https://leetcode.com/problems/missing-number/)|[Java](src/array/MissingNumber/MissingNumber.java)|Easy|
|283|[Move Zeroes](https://leetcode.com/problems/move-zeroes/)|[Java](src/array/moveZeroes/MoveZeroes.java)|Easy|
|414|[Third Maximum Number](https://leetcode.com/problems/third-maximum-number/)|[Java](src/array/thirdMaximumNumber/ThirdMaximumNumber.java)|Easy|
|448|[Find All Numbers Disappeared in an Array](https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/)|[Java](src/array/findAllNumbersDisappearedArray/FindAllNumbersDisappearedArray.java)|Easy|
|485|[Max Consecutive Ones](https://leetcode.com/problems/max-consecutive-ones/)|[Java](src/array/maxConsecutiveOnes/MaxConsecutiveOnes.java)|Easy|
|509|[Fibonacci Number](https://leetcode.com/problems/fibonacci-number/)|[Java](src/array/fibonacciNumber/FibonacciNumber.java)|Easy|
|532|[K-diff Pairs in an Array](https://leetcode.com/problems/k-diff-pairs-in-an-array/)|[Java](src/array/kdiffPairsinanArray/KdiffPairsinanArray.java)|Easy|
|561|[Array Partition I](https://leetcode.com/problems/array-partition-i/)|[Java](src/array/arrayPartition_1/ArrayPartition_1.java)|Easy|
|566|[K-diff Pairs in an Array](https://leetcode.com/problems/reshape-the-matrix/)|[Java](src/array/reshapetheMatrix/ReshapetheMatrix.java)|Easy|
|581|[Shortest Unsorted Continuous Subarray](https://leetcode.com/problems/shortest-unsorted-continuous-subarray/)|[Java](src/array/shortestUnsortedContinuousSubarray/ShortestUnsortedContinuousSubarray.java)|Easy|
|605|[Can Place Flowers](https://leetcode.com/problems/can-place-flowers/)|[Java](src/array/canPlaceFlowers/CanPlaceFlowers.java)|Easy|
