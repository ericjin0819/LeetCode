package array.longestContinuousIncreasingSubsequence;

/**
 * Create by IntelliJ IDEA.
 * Author: EricJin
 * Date: 07/10/2019 10:09 AM
 */
public class LongestContinuousIncreasingSubsequence {

    public int findLengthOfLCIS(int[] nums) {
        int max = 0, cur = 1;
        if (nums.length == 0) {
            return 0;
        }
        for (int i = 1, len = nums.length; i < len; i++) {
            if (nums[i] > nums[i - 1]) cur++;
            else {
                max = Math.max(max, cur);
                cur = 1;
            }
        }
        return Math.max(max,cur);
    }

    public int findLengthOfLCIS_2(int[] nums) {
        int max = 0, anchor = 0;
        for (int i = 0, len = nums.length; i < len; i++) {
            if (i > 0 && nums[i] <= nums[i - 1]) anchor = i;
            max = Math.max(max, i - anchor + 1);
        }
        return max;
    }
}
