package array.longestContinuousIncreasingSubsequence;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Create by IntelliJ IDEA.
 * Author: EricJin
 * Date: 07/10/2019 10:13 AM
 */
public class LongestContinuousIncreasingSubsequenceTest {

    @Test
    public void findLengthOfLCIS() {
        int[] arr = new int[] {1,3,5,4,7};
        LongestContinuousIncreasingSubsequence solution = new LongestContinuousIncreasingSubsequence();
        assertEquals(3, solution.findLengthOfLCIS(arr));
        arr = new int[]{2,2,2,2,2};
        assertEquals(1, solution.findLengthOfLCIS(arr));
        arr = new int[]{1, 3, 5, 7, 9};
        assertEquals(5,solution.findLengthOfLCIS(arr));
    }
}